const resolve = require('path').resolve
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const url = require('url')
// 注意此处为公共根路径stu，同时配置main.js中new VueRouter({base: '/stu/'...}),最后nginx里需配置location /stu {...}
const publicPath = ''

module.exports = (options = {}) => ({
  entry: {
    vendor: './src/vendor',
    index: './src/main.js'
  },
  output: {
    path: resolve(__dirname, 'dist'),
    filename: options.dev ? '[name].js' : '[name].js?[chunkhash]',
    chunkFilename: '[id].js?[chunkhash]',
    publicPath: options.dev ? '/assets/' : publicPath
  },
  module: {
    rules: [{
        test: /\.vue$/,
        use: ['vue-loader']
      },
      {
        test: /\.js$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'postcss-loader']
      },
      {
        test: /\.(png|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)(\?.+)?$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000
          }
        }]
      }
    ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest']
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),
    // 支技es新版本写法，如： **?.**可选链操作符，**??=**空值合并操作符等。安装如下：
    // npm install @babel/plugin-proposal-optional-chaining --save-dev
    // npm install @babel/plugin-proposal-nullish-coalescing-operator --save-dev
    // '@babel/plugin-proposal-optional-chaining',
    // '@babel/plugin-proposal-nullish-coalescing-operator'
  ],
  resolve: {
    alias: {
      '~': resolve(__dirname, 'src')
    },
    extensions: ['.js', '.vue', '.json', '.css']
  },
  devServer: {
    host: '0.0.0.0',
    port: 8010,
	// 代理转发
    proxy: {
      '/api/': {
        target: 'http://127.0.0.1:8770',
        changeOrigin: true,
        pathRewrite: {
          '^/api/': ''
        }
      },
      '/login': {
        target: 'http://127.0.0.1:8770',
        changeOrigin: true
      },
      '/logout': {
        target: 'http://127.0.0.1:8770',
        changeOrigin: true
      },
      // '/graphql/': {
      //   target: 'http://127.0.0.1:12800',
      //   changeOrigin: true
      // },
      // '/browser/': {
      //   target: 'http://127.0.0.1:12800',
      //   changeOrigin: true
      // },
      // '/v3/': {
      //   target: 'http://127.0.0.1:12800',
      //   changeOrigin: true
      // }
    },historyApiFallback: {
      index: url.parse(options.dev ? '/assets/' : publicPath).pathname
    }
  },
  devtool: options.dev ? '#eval-source-map' : '#source-map'
})
